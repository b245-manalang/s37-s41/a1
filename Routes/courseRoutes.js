const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const courseController = require("../Controllers/courseController.js");

// Route for creating a course
router.post("/", auth.verify, courseController.addCourse);

module.exports = router;